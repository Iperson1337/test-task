# Test task

## 1-step
```
composer install --optimize-autoloader --no-dev
```

### 2-step
```
cp .env.example .env

```
### 3-step
```
php artisan key:generate
```

### 4-step
```
npm i
```

### 5-step
```
npm run dev
```
### 6-step
```
php artisan migrate --seed
```
