<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('invoices')->group(function(){
    Route::get('/', 'InvoiceController@index');
    Route::post('/', 'InvoiceController@store')->middleware('permission:invoices-create');
    Route::put('/{invoice}', 'InvoiceController@update')->middleware('permission:invoices-update');
    Route::delete('/{invoice}', 'InvoiceController@destroy')->middleware('permission:invoices-delete');
});
